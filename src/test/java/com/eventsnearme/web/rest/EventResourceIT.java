package com.eventsnearme.web.rest;

import static com.eventsnearme.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;

import com.eventsnearme.IntegrationTest;
import com.eventsnearme.domain.Event;
import com.eventsnearme.domain.enumeration.EventFrequency;
import com.eventsnearme.domain.enumeration.EventType;
import com.eventsnearme.repository.EventRepository;
import com.eventsnearme.service.EventService;
import com.eventsnearme.service.dto.EventDTO;
import com.eventsnearme.service.mapper.EventMapper;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.util.Base64Utils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Integration tests for the {@link EventResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureWebTestClient
@WithMockUser
class EventResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final byte[] DEFAULT_PICTURE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PICTURE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PICTURE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PICTURE_CONTENT_TYPE = "image/png";

    private static final ZonedDateTime DEFAULT_WHEN = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_WHEN = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final EventType DEFAULT_TYPE = EventType.ONLINE;
    private static final EventType UPDATED_TYPE = EventType.OFFLINE;

    private static final EventFrequency DEFAULT_FREQUENCY = EventFrequency.SINGLE;
    private static final EventFrequency UPDATED_FREQUENCY = EventFrequency.DAILY;

    private static final Double DEFAULT_LOCATION_LATITUDE = 1D;
    private static final Double UPDATED_LOCATION_LATITUDE = 2D;

    private static final Double DEFAULT_LOCATION_LONGITUDE = 1D;
    private static final Double UPDATED_LOCATION_LONGITUDE = 2D;

    private static final String ENTITY_API_URL = "/api/events";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private EventRepository eventRepository;

    @Mock
    private EventRepository eventRepositoryMock;

    @Autowired
    private EventMapper eventMapper;

    @Mock
    private EventService eventServiceMock;

    @Autowired
    private WebTestClient webTestClient;

    private Event event;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Event createEntity() {
        Event event = new Event()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .picture(DEFAULT_PICTURE)
            .pictureContentType(DEFAULT_PICTURE_CONTENT_TYPE)
            .when(DEFAULT_WHEN)
            .type(DEFAULT_TYPE)
            .frequency(DEFAULT_FREQUENCY)
            .locationLatitude(DEFAULT_LOCATION_LATITUDE)
            .locationLongitude(DEFAULT_LOCATION_LONGITUDE);
        return event;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Event createUpdatedEntity() {
        Event event = new Event()
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .when(UPDATED_WHEN)
            .type(UPDATED_TYPE)
            .frequency(UPDATED_FREQUENCY)
            .locationLatitude(UPDATED_LOCATION_LATITUDE)
            .locationLongitude(UPDATED_LOCATION_LONGITUDE);
        return event;
    }

    @BeforeEach
    public void initTest() {
        eventRepository.deleteAll().block();
        event = createEntity();
    }

    @Test
    void createEvent() throws Exception {
        int databaseSizeBeforeCreate = eventRepository.findAll().collectList().block().size();
        // Create the Event
        EventDTO eventDTO = eventMapper.toDto(event);
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeCreate + 1);
        Event testEvent = eventList.get(eventList.size() - 1);
        assertThat(testEvent.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testEvent.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEvent.getPicture()).isEqualTo(DEFAULT_PICTURE);
        assertThat(testEvent.getPictureContentType()).isEqualTo(DEFAULT_PICTURE_CONTENT_TYPE);
        assertThat(testEvent.getWhen()).isEqualTo(DEFAULT_WHEN);
        assertThat(testEvent.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testEvent.getFrequency()).isEqualTo(DEFAULT_FREQUENCY);
        assertThat(testEvent.getLocationLatitude()).isEqualTo(DEFAULT_LOCATION_LATITUDE);
        assertThat(testEvent.getLocationLongitude()).isEqualTo(DEFAULT_LOCATION_LONGITUDE);
    }

    @Test
    void createEventWithExistingId() throws Exception {
        // Create the Event with an existing ID
        event.setId("existing_id");
        EventDTO eventDTO = eventMapper.toDto(event);

        int databaseSizeBeforeCreate = eventRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventRepository.findAll().collectList().block().size();
        // set the field null
        event.setTitle(null);

        // Create the Event, which fails.
        EventDTO eventDTO = eventMapper.toDto(event);

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkWhenIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventRepository.findAll().collectList().block().size();
        // set the field null
        event.setWhen(null);

        // Create the Event, which fails.
        EventDTO eventDTO = eventMapper.toDto(event);

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkLocationLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventRepository.findAll().collectList().block().size();
        // set the field null
        event.setLocationLatitude(null);

        // Create the Event, which fails.
        EventDTO eventDTO = eventMapper.toDto(event);

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void checkLocationLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventRepository.findAll().collectList().block().size();
        // set the field null
        event.setLocationLongitude(null);

        // Create the Event, which fails.
        EventDTO eventDTO = eventMapper.toDto(event);

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllEvents() {
        // Initialize the database
        eventRepository.save(event).block();

        // Get all the eventList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(event.getId()))
            .jsonPath("$.[*].title")
            .value(hasItem(DEFAULT_TITLE))
            .jsonPath("$.[*].description")
            .value(hasItem(DEFAULT_DESCRIPTION))
            .jsonPath("$.[*].pictureContentType")
            .value(hasItem(DEFAULT_PICTURE_CONTENT_TYPE))
            .jsonPath("$.[*].picture")
            .value(hasItem(Base64Utils.encodeToString(DEFAULT_PICTURE)))
            .jsonPath("$.[*].when")
            .value(hasItem(sameInstant(DEFAULT_WHEN)))
            .jsonPath("$.[*].type")
            .value(hasItem(DEFAULT_TYPE.toString()))
            .jsonPath("$.[*].frequency")
            .value(hasItem(DEFAULT_FREQUENCY.toString()))
            .jsonPath("$.[*].locationLatitude")
            .value(hasItem(DEFAULT_LOCATION_LATITUDE.doubleValue()))
            .jsonPath("$.[*].locationLongitude")
            .value(hasItem(DEFAULT_LOCATION_LONGITUDE.doubleValue()));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllEventsWithEagerRelationshipsIsEnabled() {
        when(eventServiceMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(eventServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllEventsWithEagerRelationshipsIsNotEnabled() {
        when(eventServiceMock.findAllWithEagerRelationships(any())).thenReturn(Flux.empty());

        webTestClient.get().uri(ENTITY_API_URL + "?eagerload=true").exchange().expectStatus().isOk();

        verify(eventServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    void getEvent() {
        // Initialize the database
        eventRepository.save(event).block();

        // Get the event
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, event.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(event.getId()))
            .jsonPath("$.title")
            .value(is(DEFAULT_TITLE))
            .jsonPath("$.description")
            .value(is(DEFAULT_DESCRIPTION))
            .jsonPath("$.pictureContentType")
            .value(is(DEFAULT_PICTURE_CONTENT_TYPE))
            .jsonPath("$.picture")
            .value(is(Base64Utils.encodeToString(DEFAULT_PICTURE)))
            .jsonPath("$.when")
            .value(is(sameInstant(DEFAULT_WHEN)))
            .jsonPath("$.type")
            .value(is(DEFAULT_TYPE.toString()))
            .jsonPath("$.frequency")
            .value(is(DEFAULT_FREQUENCY.toString()))
            .jsonPath("$.locationLatitude")
            .value(is(DEFAULT_LOCATION_LATITUDE.doubleValue()))
            .jsonPath("$.locationLongitude")
            .value(is(DEFAULT_LOCATION_LONGITUDE.doubleValue()));
    }

    @Test
    void getNonExistingEvent() {
        // Get the event
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewEvent() throws Exception {
        // Initialize the database
        eventRepository.save(event).block();

        int databaseSizeBeforeUpdate = eventRepository.findAll().collectList().block().size();

        // Update the event
        Event updatedEvent = eventRepository.findById(event.getId()).block();
        updatedEvent
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .when(UPDATED_WHEN)
            .type(UPDATED_TYPE)
            .frequency(UPDATED_FREQUENCY)
            .locationLatitude(UPDATED_LOCATION_LATITUDE)
            .locationLongitude(UPDATED_LOCATION_LONGITUDE);
        EventDTO eventDTO = eventMapper.toDto(updatedEvent);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, eventDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
        Event testEvent = eventList.get(eventList.size() - 1);
        assertThat(testEvent.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testEvent.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testEvent.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testEvent.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
        assertThat(testEvent.getWhen()).isEqualTo(UPDATED_WHEN);
        assertThat(testEvent.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testEvent.getFrequency()).isEqualTo(UPDATED_FREQUENCY);
        assertThat(testEvent.getLocationLatitude()).isEqualTo(UPDATED_LOCATION_LATITUDE);
        assertThat(testEvent.getLocationLongitude()).isEqualTo(UPDATED_LOCATION_LONGITUDE);
    }

    @Test
    void putNonExistingEvent() throws Exception {
        int databaseSizeBeforeUpdate = eventRepository.findAll().collectList().block().size();
        event.setId(UUID.randomUUID().toString());

        // Create the Event
        EventDTO eventDTO = eventMapper.toDto(event);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, eventDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchEvent() throws Exception {
        int databaseSizeBeforeUpdate = eventRepository.findAll().collectList().block().size();
        event.setId(UUID.randomUUID().toString());

        // Create the Event
        EventDTO eventDTO = eventMapper.toDto(event);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID().toString())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamEvent() throws Exception {
        int databaseSizeBeforeUpdate = eventRepository.findAll().collectList().block().size();
        event.setId(UUID.randomUUID().toString());

        // Create the Event
        EventDTO eventDTO = eventMapper.toDto(event);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateEventWithPatch() throws Exception {
        // Initialize the database
        eventRepository.save(event).block();

        int databaseSizeBeforeUpdate = eventRepository.findAll().collectList().block().size();

        // Update the event using partial update
        Event partialUpdatedEvent = new Event();
        partialUpdatedEvent.setId(event.getId());

        partialUpdatedEvent
            .title(UPDATED_TITLE)
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .type(UPDATED_TYPE)
            .frequency(UPDATED_FREQUENCY)
            .locationLongitude(UPDATED_LOCATION_LONGITUDE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEvent.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedEvent))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
        Event testEvent = eventList.get(eventList.size() - 1);
        assertThat(testEvent.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testEvent.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testEvent.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testEvent.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
        assertThat(testEvent.getWhen()).isEqualTo(DEFAULT_WHEN);
        assertThat(testEvent.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testEvent.getFrequency()).isEqualTo(UPDATED_FREQUENCY);
        assertThat(testEvent.getLocationLatitude()).isEqualTo(DEFAULT_LOCATION_LATITUDE);
        assertThat(testEvent.getLocationLongitude()).isEqualTo(UPDATED_LOCATION_LONGITUDE);
    }

    @Test
    void fullUpdateEventWithPatch() throws Exception {
        // Initialize the database
        eventRepository.save(event).block();

        int databaseSizeBeforeUpdate = eventRepository.findAll().collectList().block().size();

        // Update the event using partial update
        Event partialUpdatedEvent = new Event();
        partialUpdatedEvent.setId(event.getId());

        partialUpdatedEvent
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .picture(UPDATED_PICTURE)
            .pictureContentType(UPDATED_PICTURE_CONTENT_TYPE)
            .when(UPDATED_WHEN)
            .type(UPDATED_TYPE)
            .frequency(UPDATED_FREQUENCY)
            .locationLatitude(UPDATED_LOCATION_LATITUDE)
            .locationLongitude(UPDATED_LOCATION_LONGITUDE);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEvent.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedEvent))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
        Event testEvent = eventList.get(eventList.size() - 1);
        assertThat(testEvent.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testEvent.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testEvent.getPicture()).isEqualTo(UPDATED_PICTURE);
        assertThat(testEvent.getPictureContentType()).isEqualTo(UPDATED_PICTURE_CONTENT_TYPE);
        assertThat(testEvent.getWhen()).isEqualTo(UPDATED_WHEN);
        assertThat(testEvent.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testEvent.getFrequency()).isEqualTo(UPDATED_FREQUENCY);
        assertThat(testEvent.getLocationLatitude()).isEqualTo(UPDATED_LOCATION_LATITUDE);
        assertThat(testEvent.getLocationLongitude()).isEqualTo(UPDATED_LOCATION_LONGITUDE);
    }

    @Test
    void patchNonExistingEvent() throws Exception {
        int databaseSizeBeforeUpdate = eventRepository.findAll().collectList().block().size();
        event.setId(UUID.randomUUID().toString());

        // Create the Event
        EventDTO eventDTO = eventMapper.toDto(event);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, eventDTO.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchEvent() throws Exception {
        int databaseSizeBeforeUpdate = eventRepository.findAll().collectList().block().size();
        event.setId(UUID.randomUUID().toString());

        // Create the Event
        EventDTO eventDTO = eventMapper.toDto(event);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID().toString())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamEvent() throws Exception {
        int databaseSizeBeforeUpdate = eventRepository.findAll().collectList().block().size();
        event.setId(UUID.randomUUID().toString());

        // Create the Event
        EventDTO eventDTO = eventMapper.toDto(event);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Event in the database
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteEvent() {
        // Initialize the database
        eventRepository.save(event).block();

        int databaseSizeBeforeDelete = eventRepository.findAll().collectList().block().size();

        // Delete the event
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, event.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Event> eventList = eventRepository.findAll().collectList().block();
        assertThat(eventList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
