package com.eventsnearme.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import com.eventsnearme.IntegrationTest;
import com.eventsnearme.domain.EventCategory;
import com.eventsnearme.repository.EventCategoryRepository;
import com.eventsnearme.service.dto.EventCategoryDTO;
import com.eventsnearme.service.mapper.EventCategoryMapper;
import java.time.Duration;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link EventCategoryResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient
@WithMockUser
class EventCategoryResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/event-categories";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    @Autowired
    private EventCategoryRepository eventCategoryRepository;

    @Autowired
    private EventCategoryMapper eventCategoryMapper;

    @Autowired
    private WebTestClient webTestClient;

    private EventCategory eventCategory;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventCategory createEntity() {
        EventCategory eventCategory = new EventCategory().name(DEFAULT_NAME);
        return eventCategory;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EventCategory createUpdatedEntity() {
        EventCategory eventCategory = new EventCategory().name(UPDATED_NAME);
        return eventCategory;
    }

    @BeforeEach
    public void initTest() {
        eventCategoryRepository.deleteAll().block();
        eventCategory = createEntity();
    }

    @Test
    void createEventCategory() throws Exception {
        int databaseSizeBeforeCreate = eventCategoryRepository.findAll().collectList().block().size();
        // Create the EventCategory
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(eventCategory);
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        EventCategory testEventCategory = eventCategoryList.get(eventCategoryList.size() - 1);
        assertThat(testEventCategory.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    void createEventCategoryWithExistingId() throws Exception {
        // Create the EventCategory with an existing ID
        eventCategory.setId("existing_id");
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(eventCategory);

        int databaseSizeBeforeCreate = eventCategoryRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = eventCategoryRepository.findAll().collectList().block().size();
        // set the field null
        eventCategory.setName(null);

        // Create the EventCategory, which fails.
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(eventCategory);

        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    void getAllEventCategories() {
        // Initialize the database
        eventCategoryRepository.save(eventCategory).block();

        // Get all the eventCategoryList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(eventCategory.getId()))
            .jsonPath("$.[*].name")
            .value(hasItem(DEFAULT_NAME));
    }

    @Test
    void getEventCategory() {
        // Initialize the database
        eventCategoryRepository.save(eventCategory).block();

        // Get the eventCategory
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, eventCategory.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(eventCategory.getId()))
            .jsonPath("$.name")
            .value(is(DEFAULT_NAME));
    }

    @Test
    void getNonExistingEventCategory() {
        // Get the eventCategory
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewEventCategory() throws Exception {
        // Initialize the database
        eventCategoryRepository.save(eventCategory).block();

        int databaseSizeBeforeUpdate = eventCategoryRepository.findAll().collectList().block().size();

        // Update the eventCategory
        EventCategory updatedEventCategory = eventCategoryRepository.findById(eventCategory.getId()).block();
        updatedEventCategory.name(UPDATED_NAME);
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(updatedEventCategory);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, eventCategoryDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeUpdate);
        EventCategory testEventCategory = eventCategoryList.get(eventCategoryList.size() - 1);
        assertThat(testEventCategory.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    void putNonExistingEventCategory() throws Exception {
        int databaseSizeBeforeUpdate = eventCategoryRepository.findAll().collectList().block().size();
        eventCategory.setId(UUID.randomUUID().toString());

        // Create the EventCategory
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(eventCategory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, eventCategoryDTO.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchEventCategory() throws Exception {
        int databaseSizeBeforeUpdate = eventCategoryRepository.findAll().collectList().block().size();
        eventCategory.setId(UUID.randomUUID().toString());

        // Create the EventCategory
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(eventCategory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID().toString())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamEventCategory() throws Exception {
        int databaseSizeBeforeUpdate = eventCategoryRepository.findAll().collectList().block().size();
        eventCategory.setId(UUID.randomUUID().toString());

        // Create the EventCategory
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(eventCategory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateEventCategoryWithPatch() throws Exception {
        // Initialize the database
        eventCategoryRepository.save(eventCategory).block();

        int databaseSizeBeforeUpdate = eventCategoryRepository.findAll().collectList().block().size();

        // Update the eventCategory using partial update
        EventCategory partialUpdatedEventCategory = new EventCategory();
        partialUpdatedEventCategory.setId(eventCategory.getId());

        partialUpdatedEventCategory.name(UPDATED_NAME);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEventCategory.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedEventCategory))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeUpdate);
        EventCategory testEventCategory = eventCategoryList.get(eventCategoryList.size() - 1);
        assertThat(testEventCategory.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    void fullUpdateEventCategoryWithPatch() throws Exception {
        // Initialize the database
        eventCategoryRepository.save(eventCategory).block();

        int databaseSizeBeforeUpdate = eventCategoryRepository.findAll().collectList().block().size();

        // Update the eventCategory using partial update
        EventCategory partialUpdatedEventCategory = new EventCategory();
        partialUpdatedEventCategory.setId(eventCategory.getId());

        partialUpdatedEventCategory.name(UPDATED_NAME);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedEventCategory.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedEventCategory))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeUpdate);
        EventCategory testEventCategory = eventCategoryList.get(eventCategoryList.size() - 1);
        assertThat(testEventCategory.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    void patchNonExistingEventCategory() throws Exception {
        int databaseSizeBeforeUpdate = eventCategoryRepository.findAll().collectList().block().size();
        eventCategory.setId(UUID.randomUUID().toString());

        // Create the EventCategory
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(eventCategory);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, eventCategoryDTO.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchEventCategory() throws Exception {
        int databaseSizeBeforeUpdate = eventCategoryRepository.findAll().collectList().block().size();
        eventCategory.setId(UUID.randomUUID().toString());

        // Create the EventCategory
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(eventCategory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, UUID.randomUUID().toString())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamEventCategory() throws Exception {
        int databaseSizeBeforeUpdate = eventCategoryRepository.findAll().collectList().block().size();
        eventCategory.setId(UUID.randomUUID().toString());

        // Create the EventCategory
        EventCategoryDTO eventCategoryDTO = eventCategoryMapper.toDto(eventCategory);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(eventCategoryDTO))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the EventCategory in the database
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteEventCategory() {
        // Initialize the database
        eventCategoryRepository.save(eventCategory).block();

        int databaseSizeBeforeDelete = eventCategoryRepository.findAll().collectList().block().size();

        // Delete the eventCategory
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, eventCategory.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<EventCategory> eventCategoryList = eventCategoryRepository.findAll().collectList().block();
        assertThat(eventCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
