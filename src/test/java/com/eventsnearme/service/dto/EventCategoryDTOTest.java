package com.eventsnearme.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import com.eventsnearme.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EventCategoryDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EventCategoryDTO.class);
        EventCategoryDTO eventCategoryDTO1 = new EventCategoryDTO();
        eventCategoryDTO1.setId("id1");
        EventCategoryDTO eventCategoryDTO2 = new EventCategoryDTO();
        assertThat(eventCategoryDTO1).isNotEqualTo(eventCategoryDTO2);
        eventCategoryDTO2.setId(eventCategoryDTO1.getId());
        assertThat(eventCategoryDTO1).isEqualTo(eventCategoryDTO2);
        eventCategoryDTO2.setId("id2");
        assertThat(eventCategoryDTO1).isNotEqualTo(eventCategoryDTO2);
        eventCategoryDTO1.setId(null);
        assertThat(eventCategoryDTO1).isNotEqualTo(eventCategoryDTO2);
    }
}
