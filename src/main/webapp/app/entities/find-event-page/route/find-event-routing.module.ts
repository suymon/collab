import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { FindEventComponent } from '../list/find-event.component';

const findEventsRoute: Routes = [
  {
    path: '',
    component: FindEventComponent,
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(findEventsRoute)],
  exports: [RouterModule],
})
export class FindEventRoutingModule {}
