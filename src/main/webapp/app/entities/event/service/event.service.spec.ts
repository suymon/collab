import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { EventType } from 'app/entities/enumerations/event-type.model';
import { EventFrequency } from 'app/entities/enumerations/event-frequency.model';
import { IEvent, Event } from '../event.model';

import { EventService } from './event.service';

describe('Service Tests', () => {
  describe('Event Service', () => {
    let service: EventService;
    let httpMock: HttpTestingController;
    let elemDefault: IEvent;
    let expectedResult: IEvent | IEvent[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(EventService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 'AAAAAAA',
        title: 'AAAAAAA',
        description: 'AAAAAAA',
        pictureContentType: 'image/png',
        picture: 'AAAAAAA',
        when: currentDate,
        type: EventType.ONLINE,
        frequency: EventFrequency.SINGLE,
        locationLatitude: 0,
        locationLongitude: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            when: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        service.find('ABC').subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a Event', () => {
        const returnedFromService = Object.assign(
          {
            id: 'ID',
            when: currentDate.format(DATE_TIME_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            when: currentDate,
          },
          returnedFromService
        );

        service.create(new Event()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a Event', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            title: 'BBBBBB',
            description: 'BBBBBB',
            picture: 'BBBBBB',
            when: currentDate.format(DATE_TIME_FORMAT),
            type: 'BBBBBB',
            frequency: 'BBBBBB',
            locationLatitude: 1,
            locationLongitude: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            when: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a Event', () => {
        const patchObject = Object.assign(
          {
            title: 'BBBBBB',
            picture: 'BBBBBB',
            type: 'BBBBBB',
            frequency: 'BBBBBB',
            locationLongitude: 1,
          },
          new Event()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            when: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of Event', () => {
        const returnedFromService = Object.assign(
          {
            id: 'BBBBBB',
            title: 'BBBBBB',
            description: 'BBBBBB',
            picture: 'BBBBBB',
            when: currentDate.format(DATE_TIME_FORMAT),
            type: 'BBBBBB',
            frequency: 'BBBBBB',
            locationLatitude: 1,
            locationLongitude: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            when: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Event', () => {
        service.delete('ABC').subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addEventToCollectionIfMissing', () => {
        it('should add a Event to an empty array', () => {
          const event: IEvent = { id: 'ABC' };
          expectedResult = service.addEventToCollectionIfMissing([], event);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(event);
        });

        it('should not add a Event to an array that contains it', () => {
          const event: IEvent = { id: 'ABC' };
          const eventCollection: IEvent[] = [
            {
              ...event,
            },
            { id: 'CBA' },
          ];
          expectedResult = service.addEventToCollectionIfMissing(eventCollection, event);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a Event to an array that doesn't contain it", () => {
          const event: IEvent = { id: 'ABC' };
          const eventCollection: IEvent[] = [{ id: 'CBA' }];
          expectedResult = service.addEventToCollectionIfMissing(eventCollection, event);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(event);
        });

        it('should add only unique Event to an array', () => {
          const eventArray: IEvent[] = [{ id: 'ABC' }, { id: 'CBA' }, { id: 'deposit portals' }];
          const eventCollection: IEvent[] = [{ id: 'ABC' }];
          expectedResult = service.addEventToCollectionIfMissing(eventCollection, ...eventArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const event: IEvent = { id: 'ABC' };
          const event2: IEvent = { id: 'CBA' };
          expectedResult = service.addEventToCollectionIfMissing([], event, event2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(event);
          expect(expectedResult).toContain(event2);
        });

        it('should accept null and undefined values', () => {
          const event: IEvent = { id: 'ABC' };
          expectedResult = service.addEventToCollectionIfMissing([], null, event, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(event);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
