import * as dayjs from 'dayjs';
import { IEventCategory } from 'app/entities/event-category/event-category.model';
import { EventType } from 'app/entities/enumerations/event-type.model';
import { EventFrequency } from 'app/entities/enumerations/event-frequency.model';

export interface IEvent {
  id?: string;
  title?: string;
  description?: string | null;
  pictureContentType?: string | null;
  picture?: string | null;
  when?: dayjs.Dayjs;
  type?: EventType | null;
  frequency?: EventFrequency | null;
  locationLatitude?: number;
  locationLongitude?: number;
  categories?: IEventCategory[] | null;
  createdBy?: string | null;
  createdDate?: string | null;
}

export class Event implements IEvent {
  constructor(
    public id?: string,
    public title?: string,
    public description?: string | null,
    public pictureContentType?: string | null,
    public picture?: string | null,
    public when?: dayjs.Dayjs,
    public type?: EventType | null,
    public frequency?: EventFrequency | null,
    public locationLatitude?: number,
    public locationLongitude?: number,
    public categories?: IEventCategory[] | null,
    public createdBy?: string | null,
    public createdDate?: string | null
  ) {}
}

export function getEventIdentifier(event: IEvent): string | undefined {
  return event.id;
}
