export interface IEventCategory {
  id?: string;
  name?: string;
}

export class EventCategory implements IEventCategory {
  constructor(public id?: string, public name?: string) {}
}

export function getEventCategoryIdentifier(eventCategory: IEventCategory): string | undefined {
  return eventCategory.id;
}
