jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { EventCategoryService } from '../service/event-category.service';
import { IEventCategory, EventCategory } from '../event-category.model';

import { EventCategoryUpdateComponent } from './event-category-update.component';

describe('Component Tests', () => {
  describe('EventCategory Management Update Component', () => {
    let comp: EventCategoryUpdateComponent;
    let fixture: ComponentFixture<EventCategoryUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let eventCategoryService: EventCategoryService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [EventCategoryUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(EventCategoryUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(EventCategoryUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      eventCategoryService = TestBed.inject(EventCategoryService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const eventCategory: IEventCategory = { id: 'CBA' };

        activatedRoute.data = of({ eventCategory });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(eventCategory));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const eventCategory = { id: 'ABC' };
        spyOn(eventCategoryService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ eventCategory });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: eventCategory }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(eventCategoryService.update).toHaveBeenCalledWith(eventCategory);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const eventCategory = new EventCategory();
        spyOn(eventCategoryService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ eventCategory });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: eventCategory }));
        saveSubject.complete();

        // THEN
        expect(eventCategoryService.create).toHaveBeenCalledWith(eventCategory);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const eventCategory = { id: 'ABC' };
        spyOn(eventCategoryService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ eventCategory });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(eventCategoryService.update).toHaveBeenCalledWith(eventCategory);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
