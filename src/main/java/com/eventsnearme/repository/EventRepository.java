package com.eventsnearme.repository;

import com.eventsnearme.domain.Event;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data MongoDB reactive repository for the Event entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EventRepository extends ReactiveMongoRepository<Event, String> {
    Flux<Event> findAllBy(Pageable pageable);

    @Query("{}")
    Flux<Event> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    Flux<Event> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Mono<Event> findOneWithEagerRelationships(String id);
}
