package com.eventsnearme.repository;

import com.eventsnearme.domain.EventCategory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

/**
 * Spring Data MongoDB reactive repository for the EventCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EventCategoryRepository extends ReactiveMongoRepository<EventCategory, String> {
    Flux<EventCategory> findAllBy(Pageable pageable);
}
