package com.eventsnearme.service.mapper;

import com.eventsnearme.domain.*;
import com.eventsnearme.service.dto.EventDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Event} and its DTO {@link EventDTO}.
 */
@Mapper(componentModel = "spring", uses = { EventCategoryMapper.class })
public interface EventMapper extends EntityMapper<EventDTO, Event> {
    @Mapping(target = "categories", source = "categories", qualifiedByName = "nameSet")
    EventDTO toDto(Event s);

    @Mapping(target = "removeCategory", ignore = true)
    Event toEntity(EventDTO eventDTO);
}
