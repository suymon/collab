package com.eventsnearme.service;

import com.eventsnearme.domain.EventCategory;
import com.eventsnearme.repository.EventCategoryRepository;
import com.eventsnearme.service.dto.EventCategoryDTO;
import com.eventsnearme.service.mapper.EventCategoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Service Implementation for managing {@link EventCategory}.
 */
@Service
public class EventCategoryService {

    private final Logger log = LoggerFactory.getLogger(EventCategoryService.class);

    private final EventCategoryRepository eventCategoryRepository;

    private final EventCategoryMapper eventCategoryMapper;

    public EventCategoryService(EventCategoryRepository eventCategoryRepository, EventCategoryMapper eventCategoryMapper) {
        this.eventCategoryRepository = eventCategoryRepository;
        this.eventCategoryMapper = eventCategoryMapper;
    }

    /**
     * Save a eventCategory.
     *
     * @param eventCategoryDTO the entity to save.
     * @return the persisted entity.
     */
    public Mono<EventCategoryDTO> save(EventCategoryDTO eventCategoryDTO) {
        log.debug("Request to save EventCategory : {}", eventCategoryDTO);
        return eventCategoryRepository.save(eventCategoryMapper.toEntity(eventCategoryDTO)).map(eventCategoryMapper::toDto);
    }

    /**
     * Partially update a eventCategory.
     *
     * @param eventCategoryDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Mono<EventCategoryDTO> partialUpdate(EventCategoryDTO eventCategoryDTO) {
        log.debug("Request to partially update EventCategory : {}", eventCategoryDTO);

        return eventCategoryRepository
            .findById(eventCategoryDTO.getId())
            .map(
                existingEventCategory -> {
                    eventCategoryMapper.partialUpdate(existingEventCategory, eventCategoryDTO);
                    return existingEventCategory;
                }
            )
            .flatMap(eventCategoryRepository::save)
            .map(eventCategoryMapper::toDto);
    }

    /**
     * Get all the eventCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    public Flux<EventCategoryDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EventCategories");
        return eventCategoryRepository.findAllBy(pageable).map(eventCategoryMapper::toDto);
    }

    /**
     * Returns the number of eventCategories available.
     * @return the number of entities in the database.
     *
     */
    public Mono<Long> countAll() {
        return eventCategoryRepository.count();
    }

    /**
     * Get one eventCategory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    public Mono<EventCategoryDTO> findOne(String id) {
        log.debug("Request to get EventCategory : {}", id);
        return eventCategoryRepository.findById(id).map(eventCategoryMapper::toDto);
    }

    /**
     * Delete the eventCategory by id.
     *
     * @param id the id of the entity.
     * @return a Mono to signal the deletion
     */
    public Mono<Void> delete(String id) {
        log.debug("Request to delete EventCategory : {}", id);
        return eventCategoryRepository.deleteById(id);
    }
}
