package com.eventsnearme.domain.enumeration;

/**
 * The EventFrequency enumeration.
 */
public enum EventFrequency {
    SINGLE,
    DAILY,
    WEEKLY,
    MONTHLY,
    YEARLY,
}
