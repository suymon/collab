package com.eventsnearme.domain.enumeration;

/**
 * The EventType enumeration.
 */
public enum EventType {
    ONLINE,
    OFFLINE,
}
